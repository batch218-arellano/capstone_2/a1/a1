const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
mongoose.set('strictQuery', true);


//routers
const userRoute = require ("./routes/userRoute.js");
const productRoutes = require("./routes/productRoutes.js");


// express server/ application
const app = express();

app.use(cors()); 
app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.use("/users", userRoute);
app.use("/product", productRoutes);

// MongoDB Database

mongoose.connect("mongodb+srv://admin:admin@capstone.osui4cj.mongodb.net/test?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
}); 

// --- 

mongoose.connection.once('open', () => console.log('Now connected to Arellano-Mongo DB Atlas.'))

app.listen(process.env.PORT || 8000, () => 
	{console.log(`API is now online on port ${process.env.PORT || 8000 }`)
});

