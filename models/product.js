const mongoose = require ("mongoose");
const productRoutes = require("../routes/productRoutes.js");
const productSchema = new mongoose.Schema ({

	name: {

		type: String,
		required: [true, "Produce Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true, 
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [

		{
			orderId: {
				type: String,
				requred: [true, "orderId is required"]
			}

	}]

})



module.exports = mongoose.model("Product", productSchema);