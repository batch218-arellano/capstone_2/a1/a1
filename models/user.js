const mongoose = require ("mongoose");

const userSchema = new mongoose.Schema ({

	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	}, 
	IsAdmin: {
		type: Boolean,
		default: false
	}, 
	orders: [
	{
		products: [
			{
			productName: {
				type: String,
				required: [true, "Product Name is empty"]
			},
			quantity: {
				type: Number,
				default : new Number()
			},
		}],

		totalAmount: {
			type: Number,
			default: new Number()
		},
		purchasedOn : {
			type: Date,
			default: new Date()
		}

	}]
})

module.exports = mongoose.model("User", userSchema);


