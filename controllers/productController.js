const mongoose =  require("mongoose");
const Product = require ("../models/product.js");
const bcrypt = require ("bcrypt");
const auth = require("../auth.js");
// Create Product 
module.exports.addProduct = (data) => {
		console.log(data.isAdmin)

		if(data.isAdmin) {
			let newProduct = new Product({
				name: data.product.name,
				description: data.product.description,
				price: data.product.price
			})

			return newProduct.save().then((newProduct, error) => {
				if(error){
					return error;
				}

				return newProduct;

			})
		}

		let message = Promise.resolve('User must be ADMIN to access this.')

		return message.then((value) => {
			return {value}

		})

	};

// Retrieve all active products

module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result
	})
}


//retrieve single product

module.exports.retrieveProduct = (data) => {
	                    //inside the parenthesis should be the id
	return Course.findById(data).then(result => {
		return result;
	})
}

// Update Product Info (admin)

