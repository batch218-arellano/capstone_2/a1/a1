const User = require("../models/user.js");
const bcrypt = require ("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/product.js");


// User registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),

	})

	return newUser.save().then((user,error) =>{
			if (error){
				return false;
			}
			else{
				return true;
			}
		})

	}

//User Authentication

module.exports.loginAuth = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect) {
				 return {access: auth.createAccessToken(result)}
			}
			else{
				return "Username and password don't match.";
				
			}
		}

	})
}