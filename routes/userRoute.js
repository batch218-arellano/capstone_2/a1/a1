const express = require("express");
const auth = require ("../auth.js");


const router = express.Router();
const User = require("../models/user.js");
const userController = require("../controllers/userController.js");


router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req,res) => {
	userController.loginAuth(req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router;